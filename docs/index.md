# Home

## Welcome to ATLAS Software Development Tips Documentation

This documentation aims to catalog some tips for software development within the [ATLAS Collaboration](https://atlas.cern/Discover/Collaboration) at CERN. These will be updated based on the experience gained over time, so new features will be added accordingly. 🖥️👨‍💻

