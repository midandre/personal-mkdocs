# Athena Tips and Tricks

This [Athena](https://gitlab.cern.ch/atlas/athena) documentation is a compilation of tips and tricks that proved valuable during my PhD at CERN.

## Developing and Creating Merge Requests in Athena Using Sparse Checkout

Suppose you want to develop a new feature in Athena, but you only need to modify a few packages of the software. Sparse checkout is an effective method to achieve this. The following steps outline how to do it:

1. **Fork the Athena repository**:
   Follow the instructions precisely in the [ATLAS docs](https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/) to fork the Athena repo.

2. **Setting up ATLAS and adding the forked repo**:
    - **Setting up ATLAS**:
        ```bash
        setupATLAS
        lsetup git
        ```
    - **Adding the forked repo**:
        ```bash
        git atlas init-workdir ssh://git@gitlab.cern.ch:7999/atlas/athena.git -b main
        cd athena
        git checkout -b main-my-test-branch upstream/main --no-track # to checkout a new branch for later making MRs
        git push --set-upstream origin main-my-test-branch
        ```
     This **will initialize the forked repo**. The `git checkout -b` command creates a new branch called `main-my-test-branch`, and the `git push --set-upstream origin main-my-test-branch` command pushes the new branch to the forked repo.
    - **Adding the packages you want to work on**:
        ```bash
        git atlas addpkg PhysicsAnalysis/.../PackageNameYouWantToWorkOn
        ```

3. **Building Athena**:
    Now you can modify the packages you added and build Athena as usual an build the code:
    - **Create a build directory**:
        ```bash
        mkdir build
        cd build
        ```
    - **compile the code**:
        ```bash
        cmake ../athena/Projects/WorkDir
        make
        source x*/setup.sh
        ```
        In this last step, the `x*` is the name of the build directory created by the `cmake` command. **To appply the changes to the code you need to source the `setup.sh` file**.
4. **Creating a Merge Request**:
    After checking that the code compiles and runs as expected, you can push you local changes to the forked repo and create a Merge Request:
    - **Commit the changes and push**:
        ```bash
        git commit -a -m "My commit message"
        git push
        ```
        the `-a` flag stages **all** the changes and the `-m` flag adds a commit message.

    - **Create a Merge Request**:
        - Go to the forked repo in the browser and click on the `Merge Request` tab.
        - Click on the `New Merge Request` button.
        - Select the `main` branch of the original repo as the target branch and the `main-my-test-branch` as the source branch.
        - Click on the `Compare branches and continue` button.
        - Add a title and a description to the Merge Request, **mark as draft** and click on the `Submit merge request` button.
        - Ask for some checks from a colleague (the maintainer of the package you are working on) to review your code tagging them in the description of the MR (e.g. `..tagging for info @username`).
        - Once the checks are done, unmark the MR as draft. 
    - **Trigger the MR**:
        - To complete the procedure you have to do a comment in the MR to trigger the CI/CD pipeline. You can do this by adding a comment with the text `Jenkins please retry a build` in the MR.

After that you can wait for the checks to be done and for the maintainer to review your code. If everything is ok, the MR will be merged and your changes will be part of the Athena codebase.

        

## Section 2

This is the second section in the first chapter
