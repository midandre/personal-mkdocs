Updating Submodule Hash in Git

To update the hash of a submodule in Git, follow these steps:

1. Navigate to the submodule directory using the `cd` command:
   ```bash
   cd path/to/submodule
   ```

2. Switch to the desired commit within the submodule using the `git checkout` command followed by the commit hash:
   ```bash
   git checkout 674e3acac73cc4e03361dab8e6a982a456d5bb91
   ```

3. Return to the main directory of your project.

4. Update the submodule reference in the main repository by executing the `git submodule update --remote` command:
   ```bash
   git submodule update --remote
   ```

5. Commit the changes in the main repository to record the submodule reference update:
   ```bash
   git commit -am "Update submodule reference to commit 674e3acac73cc4e03361dab8e6a982a456d5bb91"
   ```

Ensure to replace `path/to/submodule` with the actual path of the submodule in your project.

With these steps, you have successfully updated the submodule hash to the specified commit in your main Git repository. If you need further clarification or assistance, feel free to ask!
